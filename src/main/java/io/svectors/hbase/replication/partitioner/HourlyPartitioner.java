package io.svectors.hbase.replication.partitioner;

import com.google.common.base.Preconditions;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * Hourly partitioner for {@link Partitioner}
 */
public class HourlyPartitioner implements Partitioner {

    protected static final String HOURLY_DATE_PATTERN = "YYYY/MM/dd/HH";
    private static final Clock UTC_CLOCK = Clock.systemUTC();

    @Override
    public String generatePartitionedPath(final String basePath, final String table) {
        Preconditions.checkNotNull(basePath);
        Preconditions.checkNotNull(table);

        final StringBuilder sb = new StringBuilder();
        sb.append(basePath).append(SEPARATOR).append(table);
        final LocalDateTime now = LocalDateTime.now(Clock.systemUTC());
        final String encodedTime = DateTimeFormatter.ofPattern(HOURLY_DATE_PATTERN).format(now);
        sb.append(SEPARATOR).append(encodedTime);
        return sb.toString();
    }

    @Override
    public long roundCeil() {
        final Instant now = Instant.now(UTC_CLOCK);
        long period =  now.truncatedTo(ChronoUnit.HOURS).plus(1, ChronoUnit.HOURS).toEpochMilli();
        return period;
    }

    @Override
    public long frequency() {
        return Duration.ofHours(1).toMillis();
    }
}
