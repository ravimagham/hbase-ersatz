package io.svectors.hbase.replication.partitioner;

import java.io.File;

/**
 * Partitioner to aid in partitioning the output.
 */
public interface Partitioner {

    String SEPARATOR = File.separator;
    String EXTENSION = "txt";
    char NAME_SEPARATOR = '-';

    /**
     * Generate the partition path for the given basepath and table name.
     * @param basePath
     * @param tableName
     * @return
     */
    String generatePartitionedPath(final String basePath, final String tableName);

    /**
     * returns the closest ceil value of the current time.
     * @return
     */
    long roundCeil();

    /**
     * returns the frequency of the partitioner in millis.
     * @return
     */
    long frequency();


    /**
     * Generate a file name.
     * @param serverName
     * @param table
     * @param suffix
     * @return
     */
    default String generateFileName(final String serverName, final String table, final String suffix) {
        return generateFileName(serverName, table, suffix, EXTENSION);
    }

    /**
     * Generate a file name.
     * @param serverName
     * @param table
     * @param suffix
     * @param extension file extension
     * @return
     */
    default String generateFileName(final String serverName, final String table, final String suffix,
                                    final String extension) {
        final StringBuilder sb = new StringBuilder();
        sb.append(serverName);
        sb.append(NAME_SEPARATOR).append(table);
        sb.append(NAME_SEPARATOR).append(suffix);
        sb.append('.').append(extension);
        return sb.toString();
    }
}
