package io.svectors.hbase.replication;

import com.lmax.disruptor.EventFactory;

/**
 * @author ravi.magham
 */
public class ErsatzEventFactory implements EventFactory<ErsatzEvent> {

    @Override
    public ErsatzEvent newInstance() {
        return new ErsatzEvent();
    }
}
