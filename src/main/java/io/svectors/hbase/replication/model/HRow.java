/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.svectors.hbase.replication.model;

import org.apache.hadoop.hbase.HConstants;
import org.apache.hadoop.hbase.util.Bytes;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Arrays;
import java.util.List;

/**
 * Maps to one row in HBase.
 * @author ravi.magham
 */
public class HRow {

    @JsonProperty("key")
	private final byte[] rowKey;

    @JsonProperty
	private final List<HColumn> columns;

    @JsonCreator
	public HRow(byte[] rowKey, HColumn... columns) {
		this(rowKey, Arrays.asList(columns));
	}

	public HRow(byte[] rowKey, List<HColumn> columns) {
		this.rowKey = rowKey;
		this.columns = columns;
	}

	public byte[] getRowKey() {
		return rowKey;
	}

	public List<HColumn> getColumns() {
		return columns;
	}

	/**
	 * Properties for a column .
	 */
	public static class HColumn {

        @JsonProperty
		private final byte[] family;

        @JsonProperty
		private final long timestamp;

        @JsonProperty
		private final byte[] qualifier;

        @JsonProperty
		private final byte[] value;

        @JsonCreator
		public HColumn(byte[] family, byte[] qualifier, byte[] value) {
			this(family, qualifier, value, HConstants.LATEST_TIMESTAMP);
		}

		public HColumn(byte[] family, byte[] qualifier, byte[] value, long timestamp) {
			this.family = family;
			this.qualifier = qualifier;
			this.value = value;
			this.timestamp = timestamp;
		}

		public byte[] getFamily() {
			return family;
		}

		public long getTimestamp() {
			return timestamp;
		}

		public byte[] getQualifier() {
			return qualifier;
		}

		public byte[] getValue() {
			return value;
		}

        @Override
        public String toString() {
            return "HColumn{" +
              "family=" + Bytes.toStringBinary(family) +
              ", timestamp=" + timestamp +
              ", qualifier=" + Bytes.toStringBinary(qualifier) +
              ", value=" + Bytes.toStringBinary(value) +
              '}';
        }
    }

    @Override
    public String toString() {
        return "HRow{" +
          "rowKey=" + Bytes.toStringBinary(rowKey) +
          ", columns=" + columns +
          '}';
    }
}
