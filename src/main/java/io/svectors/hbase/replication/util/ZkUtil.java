package io.svectors.hbase.replication.util;

import com.google.common.base.Preconditions;
import org.apache.curator.framework.CuratorFramework;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ravi.magham .
 */
public final class ZkUtil {

    private static final Logger LOG = LoggerFactory.getLogger(ZkUtil.class);
    /**
     * private default c.tor
     */
    private ZkUtil() {

    }

    /**
     * Creates the path in zookeeper
     * @param zkClient
     * @param data
     * @param path
     * @return
     */
    public static void create(final CuratorFramework zkClient, byte[] data, String... path) throws Exception {
        Preconditions.checkNotNull(zkClient);
        final String zkPath = String.join("/", path);
        try {
            zkClient.create().creatingParentContainersIfNeeded().forPath(zkPath);
        } catch (Exception e) {
            LOG.error("The zkPath {} already exists", zkPath);
        }
        zkClient.setData().forPath(zkPath, data);
    }
}
