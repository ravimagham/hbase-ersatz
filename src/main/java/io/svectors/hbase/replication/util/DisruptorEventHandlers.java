package io.svectors.hbase.replication.util;

import com.lmax.disruptor.EventHandler;
import io.svectors.hbase.replication.config.ErsatzConfiguration;
import io.svectors.hbase.replication.config.StorageConfiguration;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ravi.magham
 */
public class DisruptorEventHandlers {

    /* no public c.tor*/
    private DisruptorEventHandlers(){}

    public static EventHandler[] getHandlers(final ErsatzConfiguration configuration) {
        List<StorageConfiguration> storageConfiguration = configuration.getStorageConfiguration();
        List<EventHandler> eventHandlers = storageConfiguration.stream().map(conf -> conf.getHandler())
           .collect(Collectors.toList());
        return eventHandlers.toArray(new EventHandler[0]);
    }
}
