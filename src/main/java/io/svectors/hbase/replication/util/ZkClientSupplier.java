package io.svectors.hbase.replication.util;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import io.svectors.hbase.replication.config.ZookeeperConfiguration;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;

import java.util.concurrent.ThreadFactory;
import java.util.function.Supplier;

/**
 * @author ravi.magham
 */
public class ZkClientSupplier implements Supplier<CuratorFramework> {

	private final ZookeeperConfiguration zkConfig;

	public ZkClientSupplier(final ZookeeperConfiguration zkConfig){
		this.zkConfig = zkConfig;
	}

	@Override
	public CuratorFramework get() {
		final CuratorFrameworkFactory.Builder builder = CuratorFrameworkFactory.builder();
		builder.connectionTimeoutMs((int) zkConfig.getConnectionTimeout().toMilliseconds());
		builder.sessionTimeoutMs((int) zkConfig.getSessionTimeout().toMilliseconds());
		builder.retryPolicy(zkConfig.getRetryPolicy());
		builder.connectString(zkConfig.getQuorum());
		if (zkConfig.getNamespace().isPresent()) {
			builder.namespace(zkConfig.getNamespace().get());
		}
		final ThreadFactory threadFactory = new ThreadFactoryBuilder()
		  .setNameFormat("CuratorFramework[" + zkConfig.getQuorum() + "]-%d")
		  .setDaemon(true)
		  .build();
		builder.threadFactory(threadFactory);
		return builder.build();
	}
}
