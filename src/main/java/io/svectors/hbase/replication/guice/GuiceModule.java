package io.svectors.hbase.replication.guice;

import com.google.inject.AbstractModule;
import io.dropwizard.setup.Environment;
import io.svectors.hbase.replication.config.ErsatzConfiguration;

/**
 * @author ravi.magham
 */
public class GuiceModule extends AbstractModule {

    private final ErsatzConfiguration configuration;
    private final Environment environment;

    /**
     * argument c.tor
     * @param configuration
     * @param environment
     */
    public GuiceModule(ErsatzConfiguration configuration, Environment environment) {
        this.configuration = configuration;
        this.environment = environment;
    }


    // TODO
    @Override
    protected void configure() {
        binder().requireExplicitBindings();

    }
}
