package io.svectors.hbase.replication.disruptor;

import com.lmax.disruptor.EventTranslatorTwoArg;
import io.svectors.hbase.replication.ErsatzEvent;
import io.svectors.hbase.replication.model.HRow;

/**
 * @author ravi.magham
 */
public class ErsatzEventTranslator implements EventTranslatorTwoArg<ErsatzEvent, String, HRow> {

    public static final ErsatzEventTranslator INSTANCE = new ErsatzEventTranslator();

    /* no public c.tor*/
    private ErsatzEventTranslator(){}

    @Override
    public void translateTo(ErsatzEvent ersatzEvent, long l, String tableName, final HRow hrow) {
        ersatzEvent.of(tableName, hrow);
    }
}
