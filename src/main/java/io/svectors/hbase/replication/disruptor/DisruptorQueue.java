package io.svectors.hbase.replication.disruptor;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

import io.svectors.hbase.replication.ErsatzEvent;
import io.svectors.hbase.replication.ErsatzEventFactory;
import io.svectors.hbase.replication.ErsatzExceptionHandler;
import io.svectors.hbase.replication.Lifecylce;
import io.svectors.hbase.replication.config.ErsatzConfiguration;
import io.svectors.hbase.replication.model.HRow;
import io.svectors.hbase.replication.util.DisruptorEventHandlers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ThreadFactory;

/**
 * @author ravi.magham
 */
public class DisruptorQueue implements Lifecylce {

    private static final Logger LOG = LoggerFactory.getLogger(DisruptorQueue.class);

    private final ErsatzConfiguration configuration;
    private final ErsatzEventTranslator translator = ErsatzEventTranslator.INSTANCE;
    private Disruptor<ErsatzEvent> disruptor;
    private RingBuffer<ErsatzEvent> ringBuffer;

    public DisruptorQueue(ErsatzConfiguration configuration) {
        this.configuration = configuration;
        this.init();
    }

    @Override
    public void init() {
        final ErsatzEventFactory ersatzEventFactory = new ErsatzEventFactory();
        final int disruptorQueueSize = configuration.getQueueSize();
        final ThreadFactory threadFactory = new ThreadFactoryBuilder()
          .setNameFormat("erstaz-event-thread-%d")
          .build();

        this.disruptor = new Disruptor<>(ersatzEventFactory, disruptorQueueSize,
          threadFactory, ProducerType.MULTI, new BlockingWaitStrategy());
        final EventHandler[] eventHandlers = DisruptorEventHandlers.getHandlers(configuration);

        disruptor.setDefaultExceptionHandler(new ErsatzExceptionHandler());
        disruptor.handleEventsWith(eventHandlers);
        this.ringBuffer = disruptor.getRingBuffer();
    }

    @Override
    public void start() throws Exception {
        disruptor.start();
    }

    @Override
    public void stop() throws Exception {
        disruptor.halt();
    }

    public void enqueue(String tableName, HRow hrow) {
        ringBuffer.publishEvent(translator, tableName, hrow);
    }
}
