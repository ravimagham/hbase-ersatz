package io.svectors.hbase.replication.managed;

import io.dropwizard.lifecycle.Managed;
import org.apache.curator.framework.CuratorFramework;

/**
 * @author ravi.magham
 */
public class ManagedZookeeper implements Managed {

	private CuratorFramework curator;

	public ManagedZookeeper(final CuratorFramework curator) {
		this.curator = curator;
	}

	@Override
	public void start() throws Exception {
		this.curator.start();
	}

    @Override
	public void stop() throws Exception {
		this.curator.close();
	}
}
