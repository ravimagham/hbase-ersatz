package io.svectors.hbase.replication.managed;

import io.dropwizard.lifecycle.Managed;
import io.svectors.hbase.replication.disruptor.DisruptorQueue;

/**
 * @author ravi.magham
 */
public class ManagedDisruptorQueue implements Managed {

    private DisruptorQueue queue;

    public ManagedDisruptorQueue(final DisruptorQueue queue) {
        this.queue = queue;
    }

    @Override
    public void start() throws Exception {
        this.queue.start();
    }

    @Override
    public void stop() throws Exception {
        this.queue.stop();
    }
}
