package io.svectors.hbase.replication.managed;

import io.dropwizard.lifecycle.Managed;
import io.svectors.hbase.replication.server.ErsatzHRegionServer;

/**
 * @author ravi.magham
 */
public class ManagedErsatzServer implements Managed {

	private ErsatzHRegionServer server;

	public ManagedErsatzServer(final ErsatzHRegionServer server) {
		this.server = server;
	}

	@Override
	public void start() throws Exception {
		this.server.start();
	}

	@Override
	public void stop() throws Exception {
        this.server.stop("Shutting down server!!");
	}
}
