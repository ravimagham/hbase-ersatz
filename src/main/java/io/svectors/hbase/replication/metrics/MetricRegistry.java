package io.svectors.hbase.replication.metrics;

import com.yammer.metrics.core.MetricName;

/**
 * @author ravi.magham
 */
public class MetricRegistry {
    public static final String GROUP = "hbase-ersatz";

    private MetricRegistry() {
    }

    public static abstract class Disruptor {
        private Disruptor() {
        }

        private static final String TYPE = "disruptor";

        public static final MetricName EVENTS_SEND_RATE = new MetricName(GROUP, TYPE, "events send rate");

        public static final MetricName EVENTS_SEND_FAILURES_COUNTER = new MetricName(GROUP, TYPE, "events failed counter");

        public static final MetricName EVENTS_SEND_RETRIES_COUNTER = new MetricName(GROUP, TYPE, "Retries to send events to kafka");

        public static final MetricName ASYNC_SEND_FAILURES_COUNTER = new MetricName
            (GROUP, TYPE, "Failures to send events to kafka in async mode");

    }
}
