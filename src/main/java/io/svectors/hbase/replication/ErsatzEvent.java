package io.svectors.hbase.replication;

import io.svectors.hbase.replication.model.HRow;

/**
 * An event encapsulating the table and {@linkplain HRow}
 */
public class ErsatzEvent {

    private String tableName;
    private HRow row;

    public ErsatzEvent() {
    }

    public void of(final String tableName, final HRow row) {
        this.tableName = tableName;
        this.row = row;
    }

    public String table() {
        String _table = this.tableName;
        this.tableName = null;
        return _table;
    }

    public HRow row() {
        HRow _row = this.row;
        this.row = null;
        return _row;
    }

    @Override
    public String toString() {
        return "ErsatzEvent{" +
          "tableName='" + tableName + '\'' +
          ", row=" + row +
          '}';
    }
}
