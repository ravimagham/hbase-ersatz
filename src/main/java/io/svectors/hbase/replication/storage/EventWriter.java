package io.svectors.hbase.replication.storage;

import io.svectors.hbase.replication.EventProcessingException;
import java.io.Closeable;
import java.io.IOException;

/**
 * An EventWriter is a component that takes events from the {@link com.lmax.disruptor.EventHandler}
 * and serialize them to the destination location which can be local fs , hdfs , es or a simple logger.
 *
 * <p>A Event writer lifecycle is :</p>
 *
 * <p>Each custom {@link com.lmax.disruptor.EventHandler} consumes the events from the queue and creates an instance
 * of a EventWriter for each table name that exists in the {@link io.svectors.hbase.replication.ErsatzEvent} . The
 * EventHandler needs to invoke {@linkplain EventWriter#init()} to initialize the event writer.
 *
 * <p>The EventHanlder calls the {@link EventWriter#write(Object)} for each event consumed and the underlying writer
 * is responsible for serializing the event and write off to the destination..</p>
 *
 * @author ravi.magham
 */
public interface EventWriter<E> extends Closeable {

    /**
     * @implSpec Need to be called explicitly when the event writer is created.
     * @throws IOException
     */
    void init() throws Exception;

    /**
     * @implSpec Is the handshake method of {@link com.lmax.disruptor.EventHandler} and the event writer.
     * @param event
     * @throws IOException
     */
     void write(E event) throws EventProcessingException;
}
