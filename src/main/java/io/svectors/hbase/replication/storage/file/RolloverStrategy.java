package io.svectors.hbase.replication.storage.file;

import io.svectors.hbase.replication.RollOverException;

/**
 * @author ravi.magham
 */
public interface RolloverStrategy {

    void rollover() throws RollOverException;

}
