package io.svectors.hbase.replication.storage;

import io.svectors.hbase.replication.model.HRow;

import javax.annotation.Nullable;
import java.nio.ByteBuffer;
import java.util.TreeSet;

/**
 * Encodes the given input of type {@link HRow} to bytes
 * @author ravi.magham
 */
public interface Encoder {

    public ByteBuffer encode(@Nullable HRow input);

}
