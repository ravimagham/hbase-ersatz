package io.svectors.hbase.replication.storage.file.hdfs;

import io.svectors.hbase.replication.ErsatzEvent;
import io.svectors.hbase.replication.EventProcessingException;
import io.svectors.hbase.replication.RollOverException;
import io.svectors.hbase.replication.config.fs.FileConfiguration;
import io.svectors.hbase.replication.partitioner.Partitioner;
import io.svectors.hbase.replication.storage.file.AbstractFileEventWriter;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.HdfsConfiguration;

import java.io.IOException;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.LockSupport;

/**
 * @author ravi.magham
 * Base class for classes that wish to write to hdfs.
 */
public abstract class AbstractHdfsEventWriter<E extends ErsatzEvent> extends AbstractFileEventWriter<E>  {

    protected final Configuration hdfsConfig;
    private final String basePath;
    private final Timer rotationTimer;
    // flag to control the write. at times of rollover, we turn it off.
    private volatile AtomicBoolean writeFlag = new AtomicBoolean(true);
    private final Partitioner partitioner;
    private final AtomicInteger counter = new AtomicInteger();

    public AbstractHdfsEventWriter(final FileConfiguration fsConfiguration, final String tableName) throws Exception {
        super(fsConfiguration, tableName);
        this.hdfsConfig = getHdfsConfig(fsConfiguration);
        this.basePath = fsConfiguration.getBasePath();

        final String partitionerClassStr = fsConfiguration.getPartitioner();
        Class<? extends Partitioner> partitionerImpl =
           (Class<? extends Partitioner>)Class.forName(partitionerClassStr);
        this.partitioner = partitionerImpl.newInstance();

        TimerTask rollOverTask = new TimerTask() {
            @Override
            public void run() {
                try {
                    while(!writeFlag.compareAndSet(true, false)) {
                        LockSupport.parkUntil(TimeUnit.MILLISECONDS.toNanos(500));
                    }
                    rollover();
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                } finally {
                    writeFlag.compareAndSet(false, true);
                }

            }
        };
        rotationTimer = new Timer(true);
        long nextRollover = this.partitioner.roundCeil();
        rotationTimer.scheduleAtFixedRate(rollOverTask, TimeUnit.MINUTES.toMillis(1), nextRollover);

    }

    /**
     * Constructs the {@link Configuration} with the given config properties as specified under
     *  {@link FileConfiguration#metadata}
     *
     * @param fsConfiguration
     * @return
     */
    private Configuration getHdfsConfig(FileConfiguration fsConfiguration) {
        final Configuration configuration = new HdfsConfiguration();
        final Map<String, String> configProps = fsConfiguration.getMetadata();
        configProps.entrySet().forEach(entry -> {
            configuration.set(entry.getKey(), entry.getValue());
        });
        return configuration;
    }

    /**
     * does the write to the underlying file system.
     * @implNote We suspend write activity as long as the {@linkplain AbstractHdfsEventWriter#writeFlag} is set to true.
     *           The flag is false only during the rollover process.
     * @param e
     * @throws EventProcessingException
     */
    @Override
    public void write(E e) throws EventProcessingException {
        while(!writeFlag.get()) {
            LockSupport.parkUntil(TimeUnit.MILLISECONDS.toNanos(1000));
        }
        doWrite(e);
    }

    /**
     *
     * @throws IOException
     */
    @Override
    public void rollover() throws RollOverException {
        try {
            close();
            Path path = createOutputFile();
            initializeWriter(path);
        } catch(Exception ioex) {
            throw new RollOverException(ioex.getMessage(), ioex);
        }
    }

    protected Path createOutputFile() throws IOException {
        final String path = partitioner.generatePartitionedPath(this.basePath, this.tableName);
        final String suffix = counter.incrementAndGet() + "-" + UUID.randomUUID().toString().substring(0, 8);
        final String fileName = partitioner.generateFileName("localhost", this.tableName, suffix);
        final Path p = new Path(path,fileName);
        return p;
    }

    /**
     *
     * @param event
     * @throws IOException
     */
    public abstract void doWrite(E event)  throws EventProcessingException;

    /**
     * Initializes the writer with the path.
     * @param path
     * @throws IOException
     */
    public abstract void initializeWriter(Path path) throws EventProcessingException;

}
