package io.svectors.hbase.replication.storage.file.local;

import com.google.common.io.Closeables;
import io.svectors.hbase.replication.ErsatzEvent;
import io.svectors.hbase.replication.EventProcessingException;
import io.svectors.hbase.replication.RollOverException;
import io.svectors.hbase.replication.config.fs.FileConfiguration;
import io.svectors.hbase.replication.model.HRow;
import io.svectors.hbase.replication.partitioner.Partitioner;
import io.svectors.hbase.replication.storage.file.AbstractFileEventWriter;
import io.svectors.hbase.replication.storage.Encoder;
import org.apache.hadoop.hbase.util.ReflectionUtils;
import org.assertj.core.util.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.ThreadSafe;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @implNote This is an event writer class that performs the write of events to local file system.
 * @author ravi.magham
 */
public class LocalEventWriter<E extends ErsatzEvent> extends AbstractFileEventWriter<E> {

    private static final Logger LOG = LoggerFactory.getLogger(LocalEventWriter.class);

    /* the output path on the file system where the events will be written to.*/
    private final String basePath;
    private final String tableName;
    private final Timer rotationTimer;
    private final ByteBuffer nl = ByteBuffer.wrap("\n".getBytes());
    private final AtomicInteger counter = new AtomicInteger();
    private final Semaphore writeLock = new Semaphore(1, true);
    private final Partitioner partitioner;
    private final Encoder encoder;

    private OutputStream outputStream;
    private WritableByteChannel channel;

    /**
     *
     * @param fsConfiguration configuration parameters related to file system.
     * @param tableName the table name which comes from the raw event.
     * @throws Exception
     */
    public LocalEventWriter(final FileConfiguration fsConfiguration, final String tableName) throws Exception {
        super(fsConfiguration, tableName);
        Preconditions.checkNotNull(tableName);
        Preconditions.checkNotNull(fsConfiguration);

        this.basePath = fsConfiguration.getBasePath();
        this.tableName = tableName;
        Preconditions.checkNotNull(this.basePath);

        final String partitionerClassStr = fsConfiguration.getPartitioner();
        Class<? extends Partitioner> partitionerImpl =
          (Class<? extends Partitioner>)Class.forName(partitionerClassStr);
        this.partitioner = ReflectionUtils.newInstance(partitionerImpl);

        final String encoderClassStr = fsConfiguration.getEncoderClass();
        Class<? extends Encoder> encoderImpl =
          (Class<? extends Encoder>)Class.forName(encoderClassStr);
        this.encoder = ReflectionUtils.newInstance(encoderImpl);

        LOG.error(" in c.tor of local event writer where  the encoder is  {} and partitioner {} ",
            this.encoder.getClass().getName(), this.partitioner.getClass().getName());

        rotationTimer = new Timer(true);
        long nextRollover = this.partitioner.roundCeil();
        rotationTimer.scheduleAtFixedRate(timerTask(), nextRollover, this.partitioner.frequency());
    }

    @Override
    public void init() throws Exception {
        openChannel();
    }

    /**
     * does the write to the underlying file system.
     * @implNote We acquire the semaphore before doing the write. This is to ensure the {@linkplain LocalEventWriter#rollover()}
     *           activity when triggered doesn't begin closing the channel when the write is in flight.
     * @param event
     * @throws EventProcessingException
     */
    @Override
    public void write(ErsatzEvent event) throws EventProcessingException {
        try {
             // TODO
            // try to optimize this by not trying to acquire the mutex for each write rather start the process
            // of acquiring when coming close to the paritioner time window or look for a
            // cheap mutex locking solution.
            writeLock.acquire();
            HRow hRow = event.row();
            ByteBuffer byteBuffer = this.encoder.encode(hRow);
            this.channel.write(byteBuffer);
            this.channel.write(nl);
        } catch (InterruptedException | IOException ex) {
            LOG.error("Failed to write the event {}", event);
            throw new EventProcessingException(ex);
        } finally {
            writeLock.release();
        }
    }

    @Override
    public void close() throws IOException {
        flush();
        Closeables.close(this.channel, false);
        Closeables.close(this.outputStream, false);
        this.channel = null;
        this.outputStream = null;
    }

    @Override
    public void rollover() throws RollOverException {
        try {
            writeLock.acquire();
            close();
            openChannel();
        } catch(Exception ioex) {
            throw new RollOverException(ioex.getMessage(), ioex);
        } finally {
            writeLock.release();
        }
    }

    /**
     * Opens a channel to write to local file system.
     * @throws IOException
     */
    private void openChannel() throws IOException {
        final Path filePath = generateOutputFilePath();
        this.outputStream = new BufferedOutputStream(Files.newOutputStream(filePath));
        this.channel = Channels.newChannel(outputStream);
    }

    /**
     * generates the outputPath..
      * @throws IOException
     */
    private Path generateOutputFilePath() throws IOException {
        final String path = this.partitioner.generatePartitionedPath(basePath, tableName);
        final Path outputPath = Paths.get(path);
        if(!Files.exists(outputPath)) {
            Files.createDirectories(outputPath);
        }
        final String suffix = counter.incrementAndGet() + "-" + UUID.randomUUID().toString().substring(0, 8);
        final String fileName = this.partitioner.generateFileName("localhost", this.tableName, suffix);
        return Paths.get(path, fileName);
    }

    @Override
    public void flush() throws IOException {
        if(this.outputStream != null) {
            this.outputStream.flush();
        }
    }

    /**
     * creates the timer task which calls the rollover.
     * @return
     */
    private TimerTask timerTask() {
        TimerTask rollOverTask = new TimerTask() {
            @Override
            public void run() {
                try {
                    rollover();
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
            }
        };
        return rollOverTask;
    }
}
