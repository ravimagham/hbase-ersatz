package io.svectors.hbase.replication.storage.file;

import com.google.common.base.Preconditions;
import com.google.common.base.Throwables;

import io.svectors.hbase.replication.ErsatzEvent;
import io.svectors.hbase.replication.config.fs.FileConfiguration;
import io.svectors.hbase.replication.storage.EventWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;

/**
 * @author ravi.magham
 */
public class FileEventWriterFactory {

    private static final Logger LOG = LoggerFactory.getLogger(FileEventWriterFactory.class);

    /**
     * default c.tor
     */
    private FileEventWriterFactory() {
    }

    /**
     * Factory of {@link EventWriter} . Each table has its own writer class.
     * @param fsConfig
     * @param tableName
     * @return
     */
    public static EventWriter<ErsatzEvent> createWriter(final FileConfiguration fsConfig,
                                                        final String tableName) {
        Preconditions.checkNotNull(fsConfig);
        Preconditions.checkNotNull(tableName);
        try {
            final String eventWriterStr = fsConfig.getWriterClass();
            Class<? extends EventWriter> eventWriterClass = (Class<? extends EventWriter>)Class.forName(eventWriterStr);
            Constructor<? extends EventWriter> ctor = eventWriterClass.getConstructor(FileConfiguration.class, String.class);
            final EventWriter<ErsatzEvent> eventWriter  = ctor.newInstance(fsConfig, tableName);
            eventWriter.init();

            return eventWriter;
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(" Failed to produce a writer for table {} " , tableName);
            throw Throwables.propagate(ex);
        }
    }
}
