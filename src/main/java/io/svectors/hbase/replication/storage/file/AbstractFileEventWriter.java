package io.svectors.hbase.replication.storage.file;

import com.google.common.base.Preconditions;
import io.svectors.hbase.replication.EventProcessingException;
import io.svectors.hbase.replication.config.fs.FileConfiguration;
import io.svectors.hbase.replication.storage.EventWriter;

import java.io.Flushable;

/**
 * Base class of event writers whose destination is the file system.
 * @author ravi.magham
 */
public abstract class AbstractFileEventWriter<E> implements EventWriter<E>, RolloverStrategy, Flushable {

    protected String tableName;
    protected FileConfiguration fsConfig;

    public AbstractFileEventWriter(final FileConfiguration fsConfiguration, final String tableName)
           throws EventProcessingException {

        Preconditions.checkNotNull(fsConfiguration);
        Preconditions.checkNotNull(tableName);

        this.fsConfig = fsConfiguration;
        this.tableName = tableName;
    }
}
