package io.svectors.hbase.replication.storage.file.hdfs;

import com.google.common.base.Preconditions;
import io.svectors.hbase.replication.ErsatzEvent;
import io.svectors.hbase.replication.EventProcessingException;
import io.svectors.hbase.replication.config.fs.FileConfiguration;
import io.svectors.hbase.replication.model.HRow;
import io.svectors.hbase.replication.storage.EventWriter;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;

/**
 * A sequence file writer implementation for {@link EventWriter}
 */
public class SequenceFileEventWriter extends AbstractHdfsEventWriter<ErsatzEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(SequenceFileEventWriter.class);

    private SequenceFile.Writer writer;
    private final CompressionCodecFactory codecFactory;
    private final String keyClass;
    private final String valueClass;
    private final String compressionCodec;
    private final SequenceFile.CompressionType compressionType;

    /**
     *
     * @param fsConfiguration configuration for file system.
     * @param tableName the hbase table name
     * @throws Exception
     */
    public SequenceFileEventWriter(final FileConfiguration fsConfiguration, final String tableName)
                throws Exception {
        super(fsConfiguration, tableName);
        Preconditions.checkNotNull(tableName);
        this.keyClass = ImmutableBytesWritable.class.getName();
        this.valueClass = HRow.class.getName();
        this.compressionCodec = "default";
        this.compressionType = SequenceFile.CompressionType.BLOCK;
        this.codecFactory = new CompressionCodecFactory(hdfsConfig);
    }

    @Override
    public void initializeWriter(Path path) throws EventProcessingException {
        LOG.info(String.format(" Initializing Sequence file for the path [%s]", path.toString()));
        try {
            this.writer = SequenceFile.createWriter(
              this.hdfsConfig,
              SequenceFile.Writer.file(path),
              SequenceFile.Writer.keyClass(Class.forName(this.keyClass)),
              SequenceFile.Writer.valueClass(Class.forName(this.valueClass)),
              SequenceFile.Writer.compression(this.compressionType, this.codecFactory.getCodecByName(this.compressionCodec))
            );
        } catch (Exception ex) {
            String errorMsg = String.format("Failed with a [%s] error to initialize the writer for [%s] path ", ex.getMessage(),
              path.getName());
            throw new EventProcessingException(errorMsg, ex);
        }
    }

    @Override
    public void flush() throws IOException {
        this.writer.hsync();
    }

    @Override
    public void doWrite(ErsatzEvent event) throws EventProcessingException {
        try {
            final HRow row = event.row();
            final ImmutableBytesWritable key = new ImmutableBytesWritable(row.getRowKey());
            this.writer.append(key, row);
        }  catch (Exception ex) {
            String errorMsg = String.format("Failed with a [%s] error to write the event for table [%s] ", ex.getMessage(),
              event.table());
            throw new EventProcessingException(errorMsg, ex);
        }
    }

    @Override
    public void close() throws IOException {
        flush();
        this.writer.close();
    }

    @Override
    public void init() throws Exception {
        final Path path = createOutputFile();
        initializeWriter(path);
    }
}
