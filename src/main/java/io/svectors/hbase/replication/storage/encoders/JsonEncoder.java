package io.svectors.hbase.replication.storage.encoders;

import com.google.common.base.Preconditions;
import io.svectors.hbase.replication.model.HRow;
import io.svectors.hbase.replication.storage.Encoder;
import io.svectors.hbase.replication.storage.EncodingException;
import org.codehaus.jackson.map.ObjectMapper;

import javax.annotation.Nullable;
import java.nio.ByteBuffer;

/**
 * @author ravi.magham
 */
public class JsonEncoder implements Encoder {

    private final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * default c.tor
     */
    public JsonEncoder() {
    }

    @Override
    public ByteBuffer encode(@Nullable HRow input) {
        Preconditions.checkNotNull(input);
       try {
             return ByteBuffer.wrap(this.objectMapper.writeValueAsBytes(input));
       } catch (Exception ex) {
            throw new EncodingException("Error encoding input to a json message", ex);
       }
    }
}
