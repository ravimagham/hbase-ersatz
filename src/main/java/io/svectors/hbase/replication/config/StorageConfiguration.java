package io.svectors.hbase.replication.config;

import io.svectors.hbase.replication.handler.ErsatzEventHandler;

import java.util.Map;

/**
 * @author ravi.magham
 */
public interface StorageConfiguration {

    /**
     * All metadata attributes that need to be passed to writers.
     * @return
     */
    public Map<String, String> getMetadata();

    /**
     * Event handler for each event type
     * @return
     */
    public ErsatzEventHandler getHandler();
}
