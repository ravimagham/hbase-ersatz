package io.svectors.hbase.replication.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.collect.ImmutableList;

import io.dropwizard.Configuration;
import io.svectors.hbase.replication.config.fs.FileConfiguration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author ravi.magham
 */
public class ErsatzConfiguration  extends Configuration {

    @Valid
    @JsonProperty
    private String name;

    @Valid
    @NotNull
    @JsonSubTypes({@JsonSubTypes.Type(value = FileConfiguration.class, name = "file")})
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", include = JsonTypeInfo.As.PROPERTY)
    @JsonProperty("storage")
    private ImmutableList<StorageConfiguration> storageConfiguration = ImmutableList.of();

    @Valid
	@NotNull
	@JsonProperty("zookeeper")
	private ZookeeperConfiguration zkConfiguration;

    @Valid
    @NotNull
    @JsonProperty("queue.size")
    private int queueSize = 1024;

	@Valid
	@NotNull
	@JsonProperty("hbase")
	private HBaseConfiguration hbaseConfiguration = new HBaseConfiguration();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZookeeperConfiguration getZkConfiguration() {
		return zkConfiguration;
	}

	public void setZkConfiguration(ZookeeperConfiguration zkConfiguration) {
		this.zkConfiguration = zkConfiguration;
	}

	public HBaseConfiguration getHbaseConfiguration() {
		return hbaseConfiguration;
	}

	public void setHbaseConfiguration(HBaseConfiguration hbaseConfiguration) {
		this.hbaseConfiguration = hbaseConfiguration;
	}

    public int getQueueSize() {
        return queueSize;
    }

    public void setQueueSize(int queueSize) {
        this.queueSize = queueSize;
    }

    public ImmutableList<StorageConfiguration> getStorageConfiguration() {
        return storageConfiguration;
    }

    public void setStorageConfiguration(ImmutableList<StorageConfiguration> store) {
        this.storageConfiguration = store;
    }



}
