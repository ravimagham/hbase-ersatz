package io.svectors.hbase.replication.config.fs;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Maps;
import io.svectors.hbase.replication.config.StorageConfiguration;
import io.svectors.hbase.replication.handler.ErsatzEventHandler;
import io.svectors.hbase.replication.handler.FileEventHandler;
import io.svectors.hbase.replication.partitioner.HourlyPartitioner;
import io.svectors.hbase.replication.storage.encoders.JsonEncoder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * @author ravi.magham
 */
public class FileConfiguration implements StorageConfiguration {

    @Valid
    @NotNull
    @JsonProperty
    private String basePath;

    @Valid
    @NotNull
    @JsonProperty
    private String partitioner = HourlyPartitioner.class.getName();

    @Valid
    @NotNull
    @JsonProperty("writer")
    private String writerClass;

    /**
     * Set of properties that needs to be passed onto the writer classes.
     */
    @JsonProperty("metadata")
    private Map<String, String> metadata = Maps.newHashMap();


    @JsonProperty("encoder")
    private String encoderClass = JsonEncoder.class.getName();

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public String getPartitioner() {
        return partitioner;
    }

    public void setPartitioner(String partitioner) {
        this.partitioner = partitioner;
    }

    public String getWriterClass() {
        return writerClass;
    }

    public void setWriterClass(String writerClass) {
        this.writerClass = writerClass;
    }

    public String getEncoderClass() {
        return encoderClass;
    }

    public void setEncoderClass(String encoderClass) {
        this.encoderClass = encoderClass;
    }

    @Override
    public Map<String, String> getMetadata() {
        return this.metadata;
    }

    @Override
    public ErsatzEventHandler getHandler() {
        return new FileEventHandler(this);
    }

}
