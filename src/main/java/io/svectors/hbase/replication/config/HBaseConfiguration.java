package io.svectors.hbase.replication.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HConstants;

import javax.validation.constraints.NotNull;

/**
 * @author ravi.magham
 */
public class HBaseConfiguration {

	@NotNull
	@JsonProperty("handlers")
	private String handlers; // number of region handlers.

    @NotNull
    @JsonProperty("basePath")
    private String basePath;


	public String getHandlers() {
		return handlers;
	}

	public void setHandlers(String handlers) {
		this.handlers = handlers;
	}

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    /**
     * Returns the hbase configuration with properties being over-written from the input properties/yaml file.
     * @return
     */
	public Configuration getConfiguration() {
		final Configuration configuration = org.apache.hadoop.hbase.HBaseConfiguration.create();
		configuration.setBoolean(HConstants.REPLICATION_ENABLE_KEY, true);
		configuration.set(HConstants.REGION_SERVER_HANDLER_COUNT, getHandlers());
		return configuration;
	}

}
