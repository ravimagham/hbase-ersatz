package io.svectors.hbase.replication.config;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.dropwizard.util.Duration;
import io.dropwizard.validation.MinDuration;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.concurrent.TimeUnit;


/**
 * @author ravi.magham
 */
public class ZookeeperConfiguration {

	private static final String DEFAULT_CONNECT_STRING = "localhost:2181";
	private static final RetryPolicy DEFAULT_RETRY_POLICY = new BoundedExponentialBackoffRetry(100, 1000, 5);

	@NotNull
	@JsonProperty("quorum")
	private String quorum = DEFAULT_CONNECT_STRING;

	@NotNull
	@JsonProperty("retryPolicy")
	private RetryPolicy configRetryPolicy = DEFAULT_RETRY_POLICY;

	@NotNull
	@MinDuration(value = 1, unit = TimeUnit.MILLISECONDS)
	private Duration connectionTimeout = Duration.seconds(6);

	@NotNull
	@MinDuration(value = 1, unit = TimeUnit.MILLISECONDS)
	private Duration sessionTimeout = Duration.seconds(6);

	@JsonProperty("namespace")
	private Optional<String> namespace = Optional.empty();

	@Min(0)
	@Max(29)
	private int maxRetries = 5;

    @JsonIgnore
	public org.apache.curator.RetryPolicy getRetryPolicy() {
		return configRetryPolicy;
	}

    public String getQuorum() {
        return quorum;
    }

    public void setQuorum(String quorum) {
        this.quorum = quorum;
    }

    @JsonIgnore
	public void setNamespace(String namespace) {
		this.namespace = Optional.of(namespace);
	}

	public Duration getConnectionTimeout() {
		return connectionTimeout;
	}

	public Duration getSessionTimeout() {
		return sessionTimeout;
	}

	public Optional<String> getNamespace() {
		return namespace;
	}

	public int getMaxRetries() {
		return maxRetries;
	}

	@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
	@JsonSubTypes({
	  @JsonSubTypes.Type(value = BoundedExponentialBackoffRetry.class, name = "boundedExponentialBackoff"),
	  @JsonSubTypes.Type(value = ExponentialBackoffRetry.class, name = "exponentialBackoff"),
	  @JsonSubTypes.Type(value = RetryNTimes.class, name = "nTimes"),
	  @JsonSubTypes.Type(value = RetryUntilElapsed.class, name = "untilElapsed")
	})
	static interface RetryPolicy extends org.apache.curator.RetryPolicy {
	}

	private static final class BoundedExponentialBackoffRetry
	  extends org.apache.curator.retry.BoundedExponentialBackoffRetry
	  implements RetryPolicy {
		@JsonCreator
		public BoundedExponentialBackoffRetry(@JsonProperty("baseSleepTimeMs") int baseSleepTimeMs,
											  @JsonProperty("maxSleepTimeMs") int maxSleepTimeMs,
											  @JsonProperty("maxRetries") int maxRetries) {
			super(baseSleepTimeMs, maxSleepTimeMs, maxRetries);
		}
	}

	private static final class ExponentialBackoffRetry
	  extends org.apache.curator.retry.ExponentialBackoffRetry
	  implements RetryPolicy {
		@JsonCreator
		public ExponentialBackoffRetry(@JsonProperty("baseSleepTimeMs") int baseSleepTimeMs,
									   @JsonProperty("maxRetries") int maxRetries) {
			super(baseSleepTimeMs, maxRetries);
		}
	}

	private static final class RetryNTimes
	  extends org.apache.curator.retry.RetryNTimes
	  implements RetryPolicy {
		@JsonCreator
		public RetryNTimes(@JsonProperty("n") int n,
						   @JsonProperty("sleepMsBetweenRetries") int sleepMsBetweenRetries) {
			super(n, sleepMsBetweenRetries);
		}
	}

	private static final class RetryUntilElapsed
	  extends org.apache.curator.retry.RetryUntilElapsed
	  implements RetryPolicy {
		public RetryUntilElapsed(@JsonProperty("maxElapsedTimeMs") int maxElapsedTimeMs,
								 @JsonProperty("sleepMsBetweenRetries") int sleepMsBetweenRetries) {
			super(maxElapsedTimeMs, sleepMsBetweenRetries);
		}
	}
}
