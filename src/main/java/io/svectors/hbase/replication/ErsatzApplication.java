package io.svectors.hbase.replication;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.svectors.hbase.replication.checks.ZookeeperHealthCheck;
import io.svectors.hbase.replication.config.ErsatzConfiguration;
import io.svectors.hbase.replication.config.ZookeeperConfiguration;
import io.svectors.hbase.replication.disruptor.DisruptorQueue;
import io.svectors.hbase.replication.server.ErsatzHRegionServer;
import io.svectors.hbase.replication.managed.ManagedDisruptorQueue;
import io.svectors.hbase.replication.managed.ManagedErsatzServer;
import io.svectors.hbase.replication.managed.ManagedZookeeper;
import io.svectors.hbase.replication.util.ZkClientSupplier;
import org.apache.curator.framework.CuratorFramework;

/**
 *
 */
public class ErsatzApplication extends Application<ErsatzConfiguration> {

    private final String ERSATZ_SERVICE_NAME = "hbase-ersatz";

    public static void main(String... args) throws Exception {

        if (args.length == 0) {
            args = new String[]{"server", "src/main/config/local.yml"};
        }

        ErsatzApplication application = new ErsatzApplication();
        application.run(args);

    }

    @Override
    public String getName() {
        return ERSATZ_SERVICE_NAME;
    }

    @Override
    public void initialize(Bootstrap<ErsatzConfiguration> bootstrap) {
        super.initialize(bootstrap);
    }

    @Override
    public void run(ErsatzConfiguration ersatzConfiguration, Environment environment) throws Exception {

         // setup zookeeper
		final ZookeeperConfiguration zookeeperConfiguration = ersatzConfiguration.getZkConfiguration();
		final ZkClientSupplier zkClientSupplier = new ZkClientSupplier(zookeeperConfiguration);
		final CuratorFramework zkClient = zkClientSupplier.get();
		final ManagedZookeeper managedZookeeper = new ManagedZookeeper(zkClient);

		 // setup lmax queue
        final DisruptorQueue queue = new DisruptorQueue(ersatzConfiguration);
        final ManagedDisruptorQueue managedDisruptorQueue = new ManagedDisruptorQueue(queue);

        // add health checks.
		environment.healthChecks().register("zookeeper", new ZookeeperHealthCheck(zkClient));

        ErsatzHRegionServer server = new ErsatzHRegionServer(ersatzConfiguration, zkClient, queue);
		ManagedErsatzServer managedErsatzServer = new ManagedErsatzServer(server);

		// register the managed services.
		environment.lifecycle().manage(managedZookeeper);
		environment.lifecycle().manage(managedErsatzServer);
        environment.lifecycle().manage(managedDisruptorQueue);

	}
}
