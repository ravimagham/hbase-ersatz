package io.svectors.hbase.replication.handler;

import com.lmax.disruptor.LifecycleAware;
import io.svectors.hbase.replication.ErsatzEvent;
import io.svectors.hbase.replication.EventProcessingException;
import io.svectors.hbase.replication.config.fs.FileConfiguration;
import io.svectors.hbase.replication.storage.EventWriter;
import io.svectors.hbase.replication.storage.file.FileEventWriterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

/**
 *  Default event handlers for file based stores.
 *  @author ravi.magham
 */
public class FileEventHandler extends ErsatzEventHandler<ErsatzEvent> implements LifecycleAware {

    private static final Logger LOG = LoggerFactory.getLogger(FileEventHandler.class);

    private ConcurrentHashMap<String, EventWriter<ErsatzEvent>> writers = new ConcurrentHashMap<>();
    private final String name;
    private final FileConfiguration fsConfig;
    public FileEventHandler(final FileConfiguration fsConfig) {
        this.name = "FILE_EVENT_HANDLER";
        this.fsConfig = fsConfig;
    }

    @Override
    public void processEvent(ErsatzEvent ersatzEvent) throws EventProcessingException {
        final String tableName = ersatzEvent.table();
        final EventWriter<ErsatzEvent> eventWriter = getOrCreateWriter(tableName);
        eventWriter.write(ersatzEvent);
    }

    /**
     * returns the event writer for the given table name
     * @param tableName
     * @return
     */
    private EventWriter<ErsatzEvent> getOrCreateWriter(final String tableName) {
        EventWriter<ErsatzEvent> writer = writers.get(tableName);
        if(writer == null) {
            writer = FileEventWriterFactory.createWriter(fsConfig, tableName);
            writers.put(tableName, writer);
        }
        return writer;
    }

    @Override
    public void onStart() {
        final Thread currentThread = Thread.currentThread();
        currentThread.setName(name);
    }


    @Override
    public void onShutdown() {
        IOException shutdownException = null;
        for (EventWriter writer : writers.values()) {
            try {
                writer.close();
            } catch (IOException ex) {
                shutdownException = ex;
            }
        }
        if(shutdownException != null) {
            throw new RuntimeException(shutdownException);
        }
    }
}
