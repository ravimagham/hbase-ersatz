package io.svectors.hbase.replication.handler;

import com.lmax.disruptor.EventHandler;
import io.svectors.hbase.replication.ErsatzEvent;
import io.svectors.hbase.replication.EventProcessingException;

/**
 * @author ravi.magham
 */
public abstract class ErsatzEventHandler<E extends ErsatzEvent> implements EventHandler<E> {

    /**
     * all event handlers process the events .
     * @param event
     * @throws EventProcessingException
     */
    public abstract void processEvent(E event) throws EventProcessingException;

    @Override
    public void onEvent(E event, long l, boolean b) throws Exception {
        processEvent(event);
    }
}

