package io.svectors.hbase.replication.server;

import com.codahale.metrics.annotation.Counted;
import com.codahale.metrics.annotation.Timed;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.protobuf.RpcController;
import com.google.protobuf.ServiceException;

import io.svectors.hbase.replication.disruptor.DisruptorQueue;
import io.svectors.hbase.replication.config.ErsatzConfiguration;
import io.svectors.hbase.replication.func.ToHRowFunction;
import io.svectors.hbase.replication.model.HRow;
import io.svectors.hbase.replication.util.HostnameSupplier;

import org.apache.curator.framework.CuratorFramework;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellScanner;
import org.apache.hadoop.hbase.HConstants;
import org.apache.hadoop.hbase.ServerName;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.ipc.FifoRpcScheduler;
import org.apache.hadoop.hbase.ipc.PayloadCarryingRpcController;
import org.apache.hadoop.hbase.ipc.RpcServer;
import org.apache.hadoop.hbase.protobuf.generated.AdminProtos;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.zookeeper.ZooKeeperWatcher;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Ersatz hbase server.
 */
public class ErsatzHRegionServer extends ErsatzBaseHRegionServer {

    private static final Logger LOG = LoggerFactory.getLogger(ErsatzHRegionServer.class);

    /* to convert the raw cells to HRow */
    private final ToHRowFunction toHRowFunction = new ToHRowFunction();
    private final ErsatzConfiguration conf;
    private final CuratorFramework zkClient;
    private final DisruptorQueue queue;

    private Configuration hbaseConf;
    private ServerName serverName;
    private ZooKeeperWatcher zkWatcher;
    private volatile boolean running = false;
    private RpcServer rpcServer;
    private String rsServerPath;


    public ErsatzHRegionServer(final ErsatzConfiguration conf, final CuratorFramework zkClient, final DisruptorQueue queue)
            throws IOException, InterruptedException {

        this.conf = conf;
		this.zkClient = zkClient;
        this.queue = queue;

        this.hbaseConf = conf.getHbaseConfiguration().getConfiguration();
        final String zookeeperQuorum = conf.getZkConfiguration().getQuorum();
        this.hbaseConf.set(HConstants.ZOOKEEPER_QUORUM, zookeeperQuorum);

		final HostnameSupplier hostnameSupplier = new HostnameSupplier(hbaseConf);
		final String hostName = hostnameSupplier.get();
        int port = this.hbaseConf.getInt(HConstants.REGIONSERVER_PORT, HConstants.DEFAULT_REGIONSERVER_PORT);

		final InetSocketAddress socketAddress = new InetSocketAddress(hostName, port);
        if (socketAddress.getAddress() == null) {
            throw new IllegalArgumentException("Failed to resolve " + socketAddress);
        }

        String name = "regionserver/" + socketAddress.toString();
        this.rpcServer = new RpcServer(this, name , getServices(),
                socketAddress,
                hbaseConf,
                new FifoRpcScheduler(hbaseConf, hbaseConf.getInt(HConstants.REGION_SERVER_HANDLER_COUNT, 10)));

        this.serverName = ServerName.valueOf(hostName, socketAddress.getPort(), System.currentTimeMillis());
        this.zkWatcher = new ZooKeeperWatcher(hbaseConf, this.serverName.toString(), null);
    }

    public void start() throws Exception {
        rpcServer.start();
        initZnodes();
        this.running = true;
    }

    private List<RpcServer.BlockingServiceAndInterface> getServices() {
        List<RpcServer.BlockingServiceAndInterface> bssi = new ArrayList<RpcServer.BlockingServiceAndInterface>(1);
        bssi.add(new RpcServer.BlockingServiceAndInterface(
                AdminProtos.AdminService.newReflectiveBlockingService(this),
                AdminProtos.AdminService.BlockingInterface.class));
        return bssi;
    }

    @Override
    @Timed(name = "replicate-timer")
    @Counted(name = "replicate-request")
    public AdminProtos.ReplicateWALEntryResponse replicateWALEntry(final RpcController controller,
                                                                   final AdminProtos.ReplicateWALEntryRequest request) throws ServiceException {
        try {
             final List<AdminProtos.WALEntry> entries = request.getEntryList();
             final CellScanner cells = ((PayloadCarryingRpcController)controller).cellScanner();
             for (final AdminProtos.WALEntry wal : entries) {
                 TableName tableName = TableName.valueOf(wal.getKey().getTableName().toByteArray());
                 if (tableName == null) {
                     continue;
                 }
                 Multimap<ByteBuffer, Cell> cellsForRowKey = ArrayListMultimap.create();
                 int count = wal.getAssociatedCellCount();

                 for (int i = 0; i < count; i++) {
                     if (!cells.advance()) {
                         throw new ArrayIndexOutOfBoundsException("Expected=" + count + ", index=" + i);
                     }
                     Cell cell = cells.current();
                     ByteBuffer rowKey = ByteBuffer.wrap(cell.getRowArray(), cell.getRowOffset(), cell.getRowLength());
                     cellsForRowKey.put(rowKey, cell);
                 }

                 // process each row.
                 cellsForRowKey.asMap().entrySet().forEach( row -> {
                     final List<Cell> cellsForRow = (List<Cell>) row.getValue();
                     final HRow hrow = toHRowFunction.apply(cellsForRow);
                     // off to lmax
                     queue.enqueue(tableName.getNameAsString(), hrow);

                     // for debugging purpose
                     if(LOG.isDebugEnabled()) {
                         LOG.debug(" The event is for table {}", tableName.getNameAsString());
                         for (Cell cell : cellsForRow) {
                             LOG.debug(" the row key is {} and the cell is {}", Bytes.toStringBinary(row.getKey()), cell.toString());
                         }
                     }
                 });
             }
            return AdminProtos.ReplicateWALEntryResponse.newBuilder().build();
        } catch (IOException ie) {
            throw new ServiceException(ie);
        }
    }

   	@Override
	public void stop(String s) {
		this.zkWatcher.close();
		if (running) {
			running = false;
			rpcServer.stop();
			try {
				zkClient.delete().forPath(rsServerPath);
			} catch (Exception e) {
				if (e instanceof InterruptedException) {
					Thread.currentThread().interrupt();
				}
			}
		}
	}

    /**
     * Sets up the necessary znodes.
     */
    private void initZnodes() {
        final String basePath = conf.getHbaseConfiguration().getBasePath();
        final String name = conf.getName();  // name of this hbase server id.
        final UUID uuid = UUID.nameUUIDFromBytes(Bytes.toBytes(name));
        final String hbaseIdPath = String.join("/", basePath, "hbaseid");
        final String rsPath = String.join("/", basePath, "rs");

        // create base path
        try {
            final Stat stat = this.zkClient.checkExists().forPath(basePath);
            if(stat == null) {
                // base path doesn't exists.
                this.zkClient.create()
                    .creatingParentsIfNeeded()
                    .withMode(CreateMode.PERSISTENT)
                    .withACL(ZooDefs.Ids.OPEN_ACL_UNSAFE)
                    .forPath(hbaseIdPath, Bytes.toBytes(uuid.toString()));

                this.zkClient.create()
                    .creatingParentsIfNeeded()
                    .withMode(CreateMode.PERSISTENT)
                    .withACL(ZooDefs.Ids.OPEN_ACL_UNSAFE)
                    .forPath(rsPath);
            }
        } catch (Exception ignore) {
            LOG.info("The zkPath {} already exists", basePath);
        }

        try {
            rsServerPath = basePath + "/rs/" + serverName.getServerName();
            zkClient.create()
                .creatingParentsIfNeeded()
                .withMode(CreateMode.EPHEMERAL)
                .withACL(ZooDefs.Ids.OPEN_ACL_UNSAFE)
                .forPath(rsServerPath, null);

        } catch(Exception ex) {
            LOG.error("The rsServerPath {} already exists", rsServerPath);
        }
    }
}
