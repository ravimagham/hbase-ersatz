package io.svectors.hbase.replication.server;

import com.google.protobuf.Message;
import com.google.protobuf.RpcController;
import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.CoordinatedStateManager;
import org.apache.hadoop.hbase.Server;
import org.apache.hadoop.hbase.ServerName;
import org.apache.hadoop.hbase.client.ClusterConnection;
import org.apache.hadoop.hbase.protobuf.generated.AdminProtos;
import org.apache.hadoop.hbase.protobuf.generated.RPCProtos;
import org.apache.hadoop.hbase.zookeeper.MetaTableLocator;
import org.apache.hadoop.hbase.zookeeper.ZooKeeperWatcher;

/**
 *  Base class that mimics a {@link org.apache.hadoop.hbase.regionserver.HRegionServer}
 */
public abstract class ErsatzBaseHRegionServer implements AdminProtos.AdminService.BlockingInterface, Server,
    org.apache.hadoop.hbase.ipc.PriorityFunction {

    @Override
    public void abort(String s, Throwable throwable) {
    }

    @Override
    public AdminProtos.UpdateConfigurationResponse updateConfiguration(RpcController rpcController, AdminProtos.UpdateConfigurationRequest updateConfigurationRequest) throws ServiceException {
        return null;
    }

    @Override
    public boolean isAborted() {
        return false;
    }

    @Override
    public AdminProtos.GetRegionInfoResponse getRegionInfo(RpcController rpcController, AdminProtos.GetRegionInfoRequest getRegionInfoRequest) throws ServiceException {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public AdminProtos.GetStoreFileResponse getStoreFile(RpcController rpcController, AdminProtos.GetStoreFileRequest getStoreFileRequest) throws ServiceException {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public AdminProtos.GetOnlineRegionResponse getOnlineRegion(RpcController rpcController, AdminProtos.GetOnlineRegionRequest getOnlineRegionRequest) throws ServiceException {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public AdminProtos.OpenRegionResponse openRegion(RpcController rpcController, AdminProtos.OpenRegionRequest openRegionRequest) throws ServiceException {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public AdminProtos.CloseRegionResponse closeRegion(RpcController rpcController, AdminProtos.CloseRegionRequest closeRegionRequest) throws ServiceException {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public AdminProtos.FlushRegionResponse flushRegion(RpcController rpcController, AdminProtos.FlushRegionRequest flushRegionRequest) throws ServiceException {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public AdminProtos.SplitRegionResponse splitRegion(RpcController rpcController, AdminProtos.SplitRegionRequest splitRegionRequest) throws ServiceException {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public AdminProtos.CompactRegionResponse compactRegion(RpcController rpcController, AdminProtos.CompactRegionRequest compactRegionRequest) throws ServiceException {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public AdminProtos.MergeRegionsResponse mergeRegions(RpcController rpcController, AdminProtos.MergeRegionsRequest mergeRegionsRequest) throws ServiceException {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public AdminProtos.ReplicateWALEntryResponse replicateWALEntry(RpcController rpcController, AdminProtos.ReplicateWALEntryRequest replicateWALEntryRequest) throws ServiceException {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public AdminProtos.ReplicateWALEntryResponse replay(RpcController rpcController, AdminProtos.ReplicateWALEntryRequest replicateWALEntryRequest) throws ServiceException {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public AdminProtos.RollWALWriterResponse rollWALWriter(RpcController rpcController, AdminProtos.RollWALWriterRequest rollWALWriterRequest) throws ServiceException {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public AdminProtos.GetServerInfoResponse getServerInfo(RpcController rpcController, AdminProtos.GetServerInfoRequest getServerInfoRequest) throws ServiceException {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public AdminProtos.StopServerResponse stopServer(RpcController rpcController, AdminProtos.StopServerRequest stopServerRequest) throws ServiceException {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public AdminProtos.UpdateFavoredNodesResponse updateFavoredNodes(RpcController rpcController, AdminProtos.UpdateFavoredNodesRequest updateFavoredNodesRequest) throws ServiceException {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    /*@Override
    public AdminProtos.UpdateConfigurationResponse updateConfiguration(RpcController rpcController, AdminProtos.UpdateConfigurationRequest updateConfigurationRequest) throws ServiceException {
        throw new UnsupportedOperationException("No plans to support!!!");
    }*/

    @Override
    public int getPriority(RPCProtos.RequestHeader requestHeader, Message message) {
        return org.apache.hadoop.hbase.HConstants.NORMAL_QOS;
    }

   // @Override
    public long getDeadline(RPCProtos.RequestHeader requestHeader, Message message) {
        return 0;
    }

    @Override
    public Configuration getConfiguration() {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public ZooKeeperWatcher getZooKeeper() {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public ClusterConnection getConnection() {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public MetaTableLocator getMetaTableLocator() {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public CoordinatedStateManager getCoordinatedStateManager() {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public ServerName getServerName() {
        throw new UnsupportedOperationException("No plans to support!!!");
    }

    @Override
    public boolean isStopped() {
        throw new UnsupportedOperationException("No plans to support!!!");
    }
}
