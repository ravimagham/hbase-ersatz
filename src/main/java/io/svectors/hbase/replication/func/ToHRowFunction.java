package io.svectors.hbase.replication.func;

import io.svectors.hbase.replication.model.HRow;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;

import java.util.List;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

/**
 * @author ravi.magham
 */
public class ToHRowFunction implements Function<List<Cell>, HRow> {

    @Override
    public HRow apply(final List<Cell> cells) {

        final byte[] rowkey = CellUtil.cloneRow(cells.get(0));
        final List<HRow.HColumn> columns = cells.stream().map(cell -> {
            byte[] family = CellUtil.cloneFamily(cell);
            byte[] qualifier = CellUtil.cloneQualifier(cell);
            byte[] value = CellUtil.cloneValue(cell);
            long timestamp = cell.getTimestamp();
            final HRow.HColumn column = new HRow.HColumn(family, qualifier, value, timestamp);
            return column;
        }).collect(toList());

        final HRow hrow = new HRow(rowkey, columns);
        return hrow;
    }
}
