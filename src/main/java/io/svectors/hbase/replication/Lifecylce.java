package io.svectors.hbase.replication;

/**
 * @author ravi.magham
 */
public interface Lifecylce {

    void init();

    void start() throws Exception;

    void stop() throws Exception;
}
