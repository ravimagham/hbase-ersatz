package io.svectors.hbase.replication.checks;

import com.codahale.metrics.health.HealthCheck;
import org.apache.curator.framework.CuratorFramework;

/**
 * @author ravi.magham
 */
public class ZookeeperHealthCheck extends HealthCheck {

	private final CuratorFramework curator;

	public ZookeeperHealthCheck(final CuratorFramework curator) {
		super();
		this.curator = curator;
	}

	@Override
	protected Result check() throws Exception {
		boolean connected = curator.getZookeeperClient().isConnected();

		String description = String.format("Connect String: %s, Connected: %s",
		  curator.getZookeeperClient().getCurrentConnectionString(), connected);

		return connected ? Result.healthy(description) : Result.unhealthy(description);
	}
}
