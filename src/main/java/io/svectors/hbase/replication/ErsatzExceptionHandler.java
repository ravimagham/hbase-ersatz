package io.svectors.hbase.replication;

import com.lmax.disruptor.ExceptionHandler;

/**
 * @author ravi.magham
 */
public class ErsatzExceptionHandler implements ExceptionHandler {

    @Override
    public void handleEventException(Throwable throwable, long l, Object o) {

    }

    @Override
    public void handleOnStartException(Throwable throwable) {

    }

    @Override
    public void handleOnShutdownException(Throwable throwable) {

    }
}
