package io.svectors.hbase.replication;

/**
 * @author ravi.magham
 */
public class RollOverException extends RuntimeException {

    public RollOverException() {
        super();
    }

    public RollOverException(String message, Exception ex) {
        super(message, ex);
    }

    public RollOverException(String message) {
        super(message);
    }

    public RollOverException(Throwable cause) {
        super(cause);
    }
}
