package io.svectors.hbase.replication;

/**
 * @author ravi.magham
 */
public class EventProcessingException extends RuntimeException {

    private final String message;

    private final Throwable cause;

    public EventProcessingException(String message, Throwable cause) {
        this.message = message;
        this.cause = cause;
    }

    public EventProcessingException(Throwable cause) {
        this("Error processing wal events", cause);
    }

    public static EventProcessingException throwable(Throwable t) {
        throw new EventProcessingException(t);
    }
}
