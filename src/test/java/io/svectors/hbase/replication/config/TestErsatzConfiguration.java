package io.svectors.hbase.replication.config;

import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import io.svectors.hbase.replication.ErsatzApplication;
import org.apache.curator.test.TestingServer;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.BoundedExponentialBackoffRetry;


import java.io.IOException;


/**
 * @author ravi.magham
 */
public class TestErsatzConfiguration {

    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("local.yml");

    @ClassRule
    public static final DropwizardAppRule<ErsatzConfiguration> RULE =
      new DropwizardAppRule<>(ErsatzApplication.class, CONFIG_PATH);

    private TestingServer zooKeeperServer;
    private CuratorFramework curator;

    @Before
    public void setup() throws Exception {
        zooKeeperServer = new TestingServer();
        curator = CuratorFrameworkFactory.newClient(zooKeeperServer.getConnectString(),
          new BoundedExponentialBackoffRetry(100, 1000, 5));
        curator.start();
    }

    @After
    public void teardown() throws IOException {
        curator.close();
        zooKeeperServer.stop();
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        System.out.println(RULE.getConfiguration().getName());
    }
}
