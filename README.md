# hbase-ersatz
hbase replication service without a remote hbase cluster :) . 

## What is this service?

This is a dropwizard application that registers itself as a replication sink with zookeeper. Apparently, it runs an HBASE RPC server which I call as `ersatz` only to mean it's a fake one as it doesn't support any of true RS behaviours other than consuming mutations. 

The idea is to register custom event handlers that consume the events received on the server and write to the destination which can HDFS, local fs , ES or where ever your imagination can take you.

## Show me a high level view of how it works

 ![Ersatz_Replication_hll.png](https://bitbucket.org/repo/dkXreA/images/3430409473-Ersatz_Replication_hll.png)

## Packaging

* mvn clean package


## Deployment

* Start the service on a node using
```bash
  java -jar target/ersatz-hbase-server.jar server path/to/config.yml
```

* At HBase shell, run the following commands.

```bash
hbase> create 'test', {NAME => 'd', REPLICATION_SCOPE => '1'}
hbase> add_peer '1', "zk:2181:/hbase-slave/events"
hbase> put 'test', 'r1', 'd', 'value'
```

## TODO
* needs lot of tests to be written.
* currently, it just works for local and hdfs. need to build a SPI for loading event handlers dynamically.