#!/usr/bin/env bash
#
# Packages the project for deployment
#
set -o errexit
set -o nounset

DEPLOY_DIR=$1
cd "$(dirname "$0")"

pushd .. > /dev/null
mvn clean package
mkdir -p "${DEPLOY_DIR}"
mv target/ersatz-hbase-server.jar "${DEPLOY_DIR}"/

popd > /dev/null

